const modDiscord = require("discord.js");
const bot = new modDiscord.Client();
const fs = require("fs");
const objConfig = JSON.parse(fs.readFileSync("./config.json", "utf8"));
const FILE = "./messages.json";
const data = fs.readFileSync(FILE, {encoding:"utf8", flag:"a+"});
if (data.length > 0 ) {
	data = JSON.parse(data);
} else {
	data = [];
}

bot.on("message", objMsg => {
	if (objMsg.channel.type === "dm") {
		data.push(
			{
				user_id: objMsg.author.id,
				username: objMsg.author.username,
				discriminator: objMsg.author.discriminator,
				message: objMsg.content,
				where:objMsg.channel.id,
				timestamp: Math.floor(Date.now / 1000)
			}
		)
		fs.writeFileSync(
			FILE,
			JSON.stringify(data, null, 2)
		);
	}
});


bot.login(objConfig.bot_token);
